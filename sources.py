# LFS Package Manager, copyright (C) 2020-2023 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

import bpy
from bpy.props import StringProperty, BoolProperty, EnumProperty
import addon_utils

import os
import json
from concurrent.futures import ThreadPoolExecutor

from .logger import logger
from .package_downloader import (
    GitLabPackageDownloader, GitHubPackageDownloader, GiteaPackageDownloader, packages
)
from .utils import get_install_path, get_request, config


def get_package_source(url):
    """Get package list from a source URL, either web or local."""
    if not url:
        return []

    if (url.startswith("http://") or url.startswith("https://")):
        import requests.exceptions
        try:
            content = get_request(url)
        except requests.exceptions.InvalidURL:
            return 'INVALID'
        if content is None:
            logger.info(f"Could not load package list at {url}")
            return 'NOT_FOUND'
        try:
            return json.loads(content)
        except json.JSONDecodeError:
            return 'INVALID'

    # Heuristic: treat URLs not starting with "https?://" as local
    if not os.path.isfile(url):
        logger.info(f"Could not load package list at {url}")
        return 'NOT_FOUND'

    with open(url, 'r') as f:
        try:
            return json.load(f)
        except json.JSONDecodeError:
            return 'INVALID'


def check_package_updates():
    """Check for each package if it can be updated"""
    install_path = get_install_path()
    os.makedirs(install_path, exist_ok=True)
    addon_prefs = bpy.context.preferences.addons[__package__].preferences

    def check_package(package):
        logger.info("Checking %s..." % package['name'])
        service = package.get("service", None)
        if service == 'gitlab':
            updater = GitLabPackageDownloader(package, install_path, service)
        elif service == 'github':
            updater = GitHubPackageDownloader(package, install_path, service)
        elif service == 'gitea':
            updater = GiteaPackageDownloader(package, install_path, service)
        else:
            logger.error("Service %s not found" % package['service'])
            return

        package['updater'] = updater
        package['is_upgradable'] = updater.is_upgradable()
        package['is_installed'] = updater.is_installed()

        if "private_token" in package:
            token = package['private_token']
            if token not in addon_prefs.tokens:
                pref_token = addon_prefs.tokens.add()
                pref_token.name = token

    logger.info("Checking packages' upgradability.")
    with ThreadPoolExecutor() as executor:
        for package in packages:
            executor.submit(check_package, package)

    logger.info("Done checking packages' upgradability.")


def upgrade_packages(op, package_name, upgrade_all):
    for package in packages:
        if not upgrade_all and package['name'] != package_name:
            continue
        updater = package['updater']
        if package['is_upgradable'] == 'YES':
            logger.info("Upgrading %s..." % package['name'])
            updater.update_package()
            # Set dirty flag, to display warning if package updated
            if package['project'] == __name__ or 'pip_deps' in package:
                global is_dirty
                is_dirty = True
                if op is not None:
                    op.report(
                        {'WARNING'}, "Update complete. Please restart Blender!")
        else:
            logger.info("Package %s not upgraded" % package['name'])

    # Activate packages
    addon_utils.modules_refresh()
    if updater.activation:
        for package in packages:
            if not upgrade_all and package['name'] != package_name:
                continue
            updater = package['updater']
            if package['is_upgradable'] == 'YES':
                addon_utils.enable(updater.activation, default_set=True,
                                   persistent=True)


def update_package_list(op):
    """Update package list from servers"""
    addon_prefs = bpy.context.preferences.addons[__package__].preferences
    logger.info("Updating packages...")

    new_package_list = []

    # Get lists from each source in turn
    for source in addon_prefs.sources:
        if not source.enabled:
            continue

        # Try getting source content
        source_packages = get_package_source(source.url)
        if type(source_packages) == str:
            source.status = source_packages
            continue

        source.status = 'FOUND'

        # Get each package if not already found
        # This creates a hierarchy, where the topmost source in the list gets priority
        # XXX Is package name the best discriminator?
        for package in source_packages:
            if package["name"] not in [p["name"] for p in new_package_list]:
                package["sources"] = [source.name]
                new_package_list.append(package)
            else:
                # Find existing package
                for p in new_package_list:
                    if p["name"] == package["name"]:
                        break
                p["sources"].append(source.name)

    # Do nothing if no package found
    if not new_package_list:
        logger.info("No package found.")
        return

    packages[:] = new_package_list

    logger.info("Done updating packages.")

    if addon_prefs.auto_update:
        check_package_updates()
        upgrade_packages(op, None, True)
        logger.info("Done upgrading packages.")


def update_source_config():
    """Serialize source to config.json"""
    addon_prefs = bpy.context.preferences.addons[__package__].preferences
    config["sources"] = [
        {"name": source.name,
         "url": source.url}
        for source in addon_prefs.sources
    ]
    config.write_config()


def source_url_update(self, value):
    "Update function for the `url` property"
    update_package_list(None)
    update_source_config()


class PackageUpgradeSource(bpy.types.PropertyGroup):
    name: StringProperty(name="Name",
                         description="Name of the source",
                         default="Source")
    url: StringProperty(name="URL",
                        description="URL of the source. Can either be local or web",
                        default="https://",
                        update=source_url_update)
    enabled: BoolProperty(
        name="Enabled",
        description="Whether this source is enabled",
        default=True,
        update=source_url_update
    )
    status: EnumProperty(
        name="Status",
        description="Status of this source",
        items=(
            ('INIT', 'Not Initialized', ''),
            ('FOUND', 'Source Found', ''),
            ('NOT_FOUND', 'Source Not Found', ''),
            ('INVALID', 'Invalid Source', ''),
        ),
        default='INIT'
    )


def source_add(name="Source", url=""):
    addon_prefs = bpy.context.preferences.addons[__package__].preferences

    # Check that identical source does not exist already
    for source in addon_prefs.sources:
        if source.name == name and source.url != "" and source.url == url:
            return source

    source = addon_prefs.sources.add()
    i = 1
    while name in addon_prefs.sources:
        name = f"Source.{i:03}"
        i += 1
    source.name = name
    source.url = url

    update_source_config()

    addon_prefs.active_source = len(addon_prefs.sources) - 1
    return source


class PACKAGE_UPGRADE_OT_source_slot_add(bpy.types.Operator):
    """Add a package source"""
    bl_idname = "preferences.package_source_slot_add"
    bl_label = "Add Source"

    def execute(self, context):
        source_add()
        return {'FINISHED'}


class PACKAGE_UPGRADE_OT_source_slot_remove(bpy.types.Operator):
    """Remove a package source"""
    bl_idname = "preferences.package_source_slot_remove"
    bl_label = "Remove Source"

    def execute(self, context):
        addon_prefs = bpy.context.preferences.addons[__package__].preferences
        addon_prefs.sources.remove(addon_prefs.active_source)
        addon_prefs.active_source = max(min(addon_prefs.active_source,
                                            len(addon_prefs.sources) - 1),
                                        0)
        update_source_config()
        return {'FINISHED'}


class PACKAGE_UPGRADE_OT_source_slot_move(bpy.types.Operator):
    """Move active package source up or down"""
    bl_idname = "preferences.package_source_slot_move"
    bl_label = "Move Source Slot"

    direction: EnumProperty(
        name="Direction",
        description="Direction to move the source slot to",
        items=(('UP', "Up", ""),
               ('DOWN', "Down", "")),
        default='UP')

    def execute(self, context):
        addon_prefs = bpy.context.preferences.addons[__package__].preferences
        dst_index = (addon_prefs.active_source +
                     (-1 if self.direction == 'UP' else 1))
        if dst_index < 0 or dst_index > len(addon_prefs.sources) - 1:
            return {'CANCELLED'}
        addon_prefs.sources.move(addon_prefs.active_source, dst_index)
        addon_prefs.active_source = dst_index
        update_source_config()
        return {'FINISHED'}


class PACKAGE_UPGRADE_UL_Sources(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
        source_entry = item
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            row = layout.row()
            icon = 'CHECKBOX_HLT' if source_entry.enabled else 'CHECKBOX_DEHLT'
            row.prop(source_entry, 'enabled', text="", icon=icon, emboss=False)

            split = row.split(factor=0.2, align=True)
            split.enabled = source_entry.enabled
            split.prop(source_entry, 'name', text="", emboss=False)
            split.prop(source_entry, 'url', text="", emboss=False)
            if source_entry.status in {'NOT_FOUND', 'INVALID'}:
                sub = split.row(align=True)
                sub.alert = True
                text = ("Source not found at URL" if source_entry.status == 'NOT_FOUND'
                        else "Invalid source")
                sub.label(text=text, icon='ERROR')

            icon = 'CHECKBOX_HLT' if source_entry.enabled else 'CHECKBOX_DEHLT'
        elif self.layout_type in {'GRID'}:
            pass


classes = (
    PackageUpgradeSource,
    PACKAGE_UPGRADE_OT_source_slot_add,
    PACKAGE_UPGRADE_OT_source_slot_remove,
    PACKAGE_UPGRADE_OT_source_slot_move,
    PACKAGE_UPGRADE_UL_Sources,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
