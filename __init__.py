# LFS Package Manager, copyright (C) 2020-2023 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


bl_info = {
    "name": "LFS Package Manager",
    "author": "Les Fées Spéciales (LFS)",
    "version": (0, 4, 2),
    "blender": (2, 80, 0),
    "location": "User Preferences > LFS Package Manager",
    "tracker_url": "https://gitlab.com/lfs.coop/blender/lfs-blender-package-manager/-/issues",
    "description": "Update lists of add-ons from online sources",
    "doc_url": "",
    "category": "Pipeline",
}

import bpy
from bpy.props import StringProperty, BoolProperty, EnumProperty, CollectionProperty, IntProperty
import addon_utils

import os
from concurrent.futures import ThreadPoolExecutor

from .logger import logger
from .package_downloader import packages
from .utils import config, get_install_path

from . import sources


DEBUG = False

is_dirty = False  # If a package was upgraded, display warning message


def destination_update(_self, _context):
    '''Update destination directory, where add-ons are stored'''
    install_path = get_install_path()

    def check_package(package, updater):
        package['is_upgradable'] = updater.is_upgradable()
        package['is_installed'] = updater.is_installed()

    with ThreadPoolExecutor() as executor:
        for package in packages:
            if "updater" not in package:
                # Package list has not yet been updated
                continue
            updater = package['updater']
            updater.install_path = install_path
            executor.submit(check_package, package, updater)

    # Write config
    addon_prefs = bpy.context.preferences.addons[__name__].preferences
    config['destination_type'] = addon_prefs.destination_type
    config['destination'] = addon_prefs.destination
    config.write_config()


def token_update(self, _context):
    '''Store tokens into the config file

    This allows tokens to persist even if the package manager is disabled.
    '''
    config_tokens = config.get('tokens', {})
    addon_prefs = bpy.context.preferences.addons[__name__].preferences
    for token in addon_prefs.tokens:
        config_tokens[token.name] = token.token
    config['tokens'] = config_tokens
    config.write_config()


class Token(bpy.types.PropertyGroup):
    token: StringProperty(name="Token",
                          description="Authentication token for private repository",
                          update=token_update)


def get_token_list():
    """
    Refresh list of tokens from config file
    """
    addon_prefs = bpy.context.preferences.addons[__name__].preferences
    for token_name in dict(config.get('tokens', {})):
        if token_name in addon_prefs.tokens:
            prefs_token = addon_prefs.tokens.get(token_name)
        else:
            prefs_token = addon_prefs.tokens.add()
        prefs_token.name = token_name
        prefs_token.token = config['tokens'][token_name]
    if "tokens" in config and "" in config['tokens']:
        config['tokens'].pop("")


class PackageUpgradePreferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    auto_update: BoolProperty(
        name="Auto-update",
        description="Automatically update available packages",
        default=False)
    destination_type: EnumProperty(
        name="Destination Type",
        description="Where to install downloaded add-ons",
        items=(
            ('USER', 'User Directory', 'Use the default Blender user directory'),
            ('PREFERENCES', 'From Preferences',
             'Use the directory set in the scripts file paths in Blender’s user preferences'),
            ('CUSTOM', 'Custom', 'Choose a custom directory')
        ),
        default='USER',
        update=destination_update)
    destination: bpy.props.StringProperty(
        name='Destination',
        maxlen=1024,
        subtype='DIR_PATH',
        update=destination_update)
    tokens: CollectionProperty(
        name="Tokens",
        description="Tokens used for private repositories authentication",
        type=Token)
    filter: bpy.props.StringProperty(
        name='Package Filter',
        maxlen=1024,
        options={'TEXTEDIT_UPDATE'})
    tab: bpy.props.EnumProperty(
        name="Tab",
        items=[("PACKAGES", "Packages", "Packages to install"),
               ("SOURCES", "Sources", "List of sources from which to get packages"),
               ],
        default="PACKAGES")

    sources: bpy.props.CollectionProperty(type=sources.PackageUpgradeSource)
    active_source: IntProperty()

    def draw(self, context):
        layout = self.layout

        # TAB BAR, ripped off from mesh_snap_utilities_line
        row = layout.row()
        row.prop(self, "tab", expand=True)

        layout.separator()

        if self.tab == "PACKAGES":
            box = layout.box()
            self.draw_packages(context, box)

        elif self.tab == "SOURCES":
            self.draw_sources(context, layout)

    def draw_packages(self, context, layout):
        row = layout.row()
        row.prop(self, "auto_update")

        col = row.column(align=True)
        col.prop(self, "destination_type", text="Destination")
        if self.destination_type == 'CUSTOM':
            col.prop(self, "destination", text="")
        else:
            col.label(text=get_install_path())

        col = layout.column(align=True)
        col.operator("preferences.package_update_list", icon="FILE_REFRESH")
        upgrade_row = col.row(align=True)

        if is_dirty:
            col = layout.column(align=True)
            row = col.row()
            row.alignment = 'CENTER'
            row.alert = True
            row.label(
                text="Update complete. Please restart Blender!", icon='ERROR')

        # Packages
        show_upgrade_all = False
        if len(packages):
            col = layout.column(align=True)
            col.prop(self, "filter", text="", icon="VIEWZOOM")

            # Get categories for each package
            categories = {'': []}
            for package in packages:
                if not (self.filter.lower() in package['name'].lower()
                        or any(self.filter.lower() in c.lower()
                               for c in package['categories'])):
                    continue
                if package['categories']:
                    for category in package['categories']:
                        if category in categories:
                            categories[category].append(package)
                        else:
                            categories[category] = [package]
                else:
                    categories[''].append(package)
            if not categories['']:
                del categories['']

            for category_name in sorted(categories):
                category = categories[category_name]
                box = col.box()
                row = box.row()
                row.alignment = 'CENTER'
                if category_name != '':
                    row.label(text=category_name)

                ops_split = box.split()

                enable_split = ops_split.split(factor=0.05, align=True)
                enable_col = enable_split.column(align=True)
                info_col = enable_split.column(align=True)

                ops_col = ops_split.column(align=True)

                blender_addons = [
                    a.__name__ for a in addon_utils.modules(refresh=False)]
                prefs = context.preferences
                used_ext = {ext.module for ext in prefs.addons}

                for package in sorted(category, key=lambda p: p['name']):
                    module_name = package['activation']
                    is_enabled = module_name in used_ext
                    if module_name in blender_addons:
                        enable_col.operator(
                            "preferences.addon_disable" if is_enabled
                            else "preferences.addon_enable",
                            icon='CHECKBOX_HLT' if is_enabled else 'CHECKBOX_DEHLT', text="",
                            emboss=False,
                        ).module = module_name
                    else:
                        enable_col.label(text="")

                    info_row = info_col.row(align=True)
                    info_row.label(text=package['name'])
                    sub = info_row.row(align=True)
                    sub.alignment = 'RIGHT'
                    sub.label(text=package["sources"][0])
                    if len(package["sources"]) > 1:
                        subsub = sub.row(align=True)
                        subsub.enabled = False
                        subsub.alignment = 'RIGHT'
                        subsub.label(
                            text=", " + ", ".join(package["sources"][1:]))

                    # if "pip_deps" in package:
                    #     row.label(text="Pip dependencies", icon="QUESTION")

                    row = ops_col.row(align=True)
                    if 'updater' in package:
                        is_upgradable = package["is_upgradable"]
                        is_installed = (package['updater'].is_installed is True
                                        or (callable(package['updater'].is_installed)
                                            and package['updater'].is_installed()))
                        sub = row.row(align=True)
                        sub.active = is_upgradable == 'YES'
                        if is_upgradable == 'YES':
                            op = sub.operator("preferences.package_upgrade",
                                              icon="FILE_REFRESH" if is_installed
                                              else "IMPORT",
                                              text="Upgrade package" if is_installed
                                              else "Install package")
                            show_upgrade_all = True
                        elif is_upgradable == 'NOT_FOUND':
                            op = sub.operator(
                                "preferences.package_upgrade", text="Package not found", icon='ERROR')
                        elif is_upgradable == 'NOT_AVAILABLE':
                            op = sub.operator(
                                "preferences.package_upgrade", text="Package not available", icon='ERROR')
                        else:
                            op = sub.operator(
                                "preferences.package_upgrade", text="Package up to date")
                        op.package = package['name']
                        if is_installed:
                            op = row.operator(
                                "preferences.package_remove", text="", icon="TRASH")
                            op.package = package['name']
                            if "installation" in package and package['installation']:
                                op = row.operator(
                                    "preferences.addon_show", icon="SETTINGS")
                                op.module = package['activation']
                    else:
                        row.alert = True
                        row.alignment = 'CENTER'
                        row.label(text="Please update package list")

            upgrade_row.active = show_upgrade_all
            if show_upgrade_all:
                op = upgrade_row.operator(
                    "preferences.package_upgrade", text="Upgrade all packages", icon="FILE_REFRESH")
            else:
                op = upgrade_row.operator(
                    "preferences.package_upgrade", text="All packages up to date")
            op.upgrade_all = True

        # Tokens
        col = layout.column()
        if len(self.tokens):
            box = col.box()
            box.label(text="Tokens")
            split = box.split()
            name_col = split.column(align=True)
            token_col = split.column(align=True)
            for token in self.tokens:
                name_col.label(text=token.name)
                token_col.prop(token, "token")

    def draw_sources(self, context, layout):
        row = layout.row()
        row.template_list('PACKAGE_UPGRADE_UL_Sources',
                          '',
                          self, "sources",
                          self, 'active_source')

        col = row.column(align=True)
        col.operator("preferences.package_source_slot_add",
                     icon='ADD', text="")

        col.operator("preferences.package_source_slot_remove",
                     icon='REMOVE', text="")

        col.separator()

        col.operator("preferences.package_source_slot_move",
                     icon='TRIA_UP', text="").direction = 'UP'
        col.operator("preferences.package_source_slot_move",
                     icon='TRIA_DOWN', text="").direction = 'DOWN'


class PREFERENCES_OT_Package_Update_List(bpy.types.Operator):
    """Update list of updatable packages"""
    bl_idname = "preferences.package_update_list"
    bl_label = "Update package list"

    def execute(self, context):
        sources.update_package_list(self)
        sources.check_package_updates()
        return {'FINISHED'}


class PREFERENCES_OT_Package_Upgrade(bpy.types.Operator):
    """Upgrade single package"""
    bl_idname = "preferences.package_upgrade"
    bl_label = "Upgrade package"

    package: StringProperty()
    upgrade_all: BoolProperty()

    def execute(self, context):
        sources.upgrade_packages(self, self.package, self.upgrade_all)
        return {'FINISHED'}


class PREFERENCES_OT_Package_Remove(bpy.types.Operator):
    """Remove package"""
    bl_idname = "preferences.package_remove"
    bl_label = "Remove package"

    package: StringProperty()

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)

    def execute(self, context):
        for package in packages:
            if package['name'] == self.package:
                break
        logger.info("Removing package %s..." % self.package)

        package['updater'].remove()

        package['is_installed'] = False
        package['is_upgradable'] = 'YES'
        addon_utils.modules_refresh()

        return {'FINISHED'}


classes = (
    PREFERENCES_OT_Package_Update_List,
    PREFERENCES_OT_Package_Upgrade,
    PREFERENCES_OT_Package_Remove,
    Token,
    PackageUpgradePreferences,
)


def do_versioning():
    """Update the config file from the old version"""
    addon_prefs = bpy.context.preferences.addons[__name__].preferences

    if "package_list_url" in config:
        sources.source_add(name="Old Source", url=config["package_list_url"])
        del config["package_list_url"]

    if "install_path" in config:
        addon_prefs.destination_type = "CUSTOM"
        addon_prefs.destination = config["install_path"]
        del config["install_path"]

    config.write_config()


def register():
    sources.register()

    for cls in classes:
        bpy.utils.register_class(cls)

    do_versioning()

    # Read sources from config
    if "sources" in config:
        config_sources = config["sources"]

        for source in config_sources:
            if "name" in source and "url" in source:
                sources.source_add(source["name"], source["url"])

    sources.update_package_list(None)

    # Load config
    addon_prefs = bpy.context.preferences.addons[__name__].preferences
    for preference in ("destination_type", "destination"):
        if preference in config:
            setattr(addon_prefs, preference, config[preference])

    get_token_list()


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)

    sources.unregister()
