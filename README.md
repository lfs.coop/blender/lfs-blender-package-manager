# Les Fées Spéciales update add-on for Blender

This Blender add-on fetches, installs and updates scripts and
dependencies.


## Installing
Download this add-on in zip format, and install it from Blender’s User
Preferences.


## Usage
Go to the add-on’s user preferences, search for its name (_LFS Package
Manager_), and after activating it, unfold its preferences by clicking
the arrow to the left of the name.

A list of packages appears, with a button to install or update the
package, as well as a button to install them all at once.


## Configuration

### Destination
You can choose where the add-ons will be installed. By default, they
go into Blender’s scripts directory, but this can be overridden by
either the user preferences .., or a custom path.

### Add-on sources
Add-ons can be installed from various sources, accessible from the
Sources tab. Each source can be either a local path, or a web URL.

A source is a json file containing information about add-ons to
install. See Package options, below.

If a package is found in multiple sources, only the first one will be
used, so the order of sources matters.

Sources are stored and loaded from the config.json file, so that
default list of sources may be given to artists when setting up a
project.


### Package options ###

The packages json file, pointed to by the latter option, contains
these fields:

``` json
[
    {
        "name": "Camera plane",
        "platform": ["Linux", "Windows", "Darwin"],
        "service": "gitlab",
        "group": "lfs.coop/blender",
        "project": "camera-plane",
        "ref_type": "branch",
        "ref": "master",
        "activation": "camera-plane",
        "categories": ["Layout"],
        "files": {"addons": ["*.py"]}
    },
    {
    ...
    }
]
```

- `name`: A name for this add-on. Should generally be the same as the
  actual add-on, but is only used in the GUI.
- `platform`: Optional list of platform names, for add-ons available
  only on specific OSs. The names are those returned by Python’s
  [`platform.system()`](https://docs.python.org/3/library/platform.html#platform.system).
- `service`: The website hosting the add-on. Currently supported
  services are gitlab and github.
- `group`: The URL subpath to the project. On GitHub, this would be a
  user or organisation. On GitLab, additional group and subgroup may
  be used.
- `project`: The name of the project on the forge.
- `ref_type`: Type of git ref to use for download. May be `branch` or `commit`.
- `ref`: The git ref to download.
  - If `ref_type` is `branch`, the add-on will query the latest commit
    in this branch from the forges’s API, and get that commit.
  - If `ref_type` is `commit`, it will be directly downloaded.
- `activation`: The add-on name, either `<addon>.py` or `<addon>/`
  directory. This is used to show an activation checkbox next to the
  add-on’s name in the list.
- `categories`: A way to group add-ons by category in the UI. If the
  list is empty, the add-on will go into a nameless category.
- `files`: A dictionary of destinations in the Blender script
  directory, and for each, a list of files to copy there. Paths should
  be full zip paths, so a glob may be used to specify them more easily.
- `pip_deps`: A list of Python dependencies to download using `pip`.

## License

Blender scripts published by **Les Fées Spéciales** are, except where
otherwise noted, licensed under the GPLv2 license.
